#!/bin/python3
import subprocess, click

def run_process(arg_list):
    p = subprocess.run(arg_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if p.returncode != 0:
        error(p.stdout.decode())
    return p.stdout.decode()

def list(dev):
    aps = {}
    lst = run_process(['iw', dev, 'scan']).split("\n")
    del lst[-1]
    lastsignal = ""
    longhest = 1
    for i in lst:
        if "signal" in i:
            signal = int(i.split(":")[-1][1:].split(".")[0])
            lastsignal = signal
        if "SSID" in i:
            if i == "" or "SSID List" in i:
                continue
            name = i.split(":")[-1][1:].replace("\x20", "")
            if "\x00" in name:
                name = "Hidden Network"
            if len(name) > longhest:
                longhest = len(name)
            if name not in aps.keys():
                aps[name] = {"Signal":lastsignal, "Lenght":len(name)}
    sorted_by_signal = sorted(aps, key=lambda x: aps[x]["Signal"], reverse=True)
    for i in sorted_by_signal:
        spaces = " " * (longhest - len(i) + 1)
        print(i+spaces+str(aps[i]["Signal"])+".00 dBm")
    return aps
def start(dev):
    run_process(["wpa_supplicant", "-i", dev, "-c", "/etc/wpa_supplicant/wpa_supplicant.conf", "-B", "-t", "-f", "/var/log/wpa_supplicant.log"])
def stop(dev):
    pid = get_process(dev)
    if pid != 0:
        run_process(['kill', pid])
    else:
        error("wpa_supplicant: Not running")
def status(dev):
    if get_process(dev) != 0:
        print("wpa_supplicant: Running")
        ap = get_ap(dev)
        print("Connected to: "+ap)
    else:
        print("wpa_supplicant: Not running")

def get_process(dev):
    p = subprocess.run(["ps", "-aux"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if p.returncode != 0:
        error(r.stdout.decode())
    processes = p.stdout.decode().split("\n")
    for i in processes:
        if "wpa_supplicant" in i and dev in i:
            return i.split()[1]#Return PID
    return 0
def get_ap(dev):
    return run_process(["iw", "dev", dev, "info"]).split("\n")[4].split(" ")[-1]
def error(desc):
    print("An error occured:")
    print(desc)
    exit()

@click.command()
@click.argument('command')
@click.argument('device')

def main(command, device):
    if command == "list":
        list(device)
    if command == "start":
        start(device)
    if command == "stop":
        stop(device)
    if command == "status":
        status(device)

if __name__=="__main__":
    main()
