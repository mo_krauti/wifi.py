# wifi.py
wifi.py is manages wpa_supplicant processes and list neary APs

## Installation
```
git clone https://gitlab.com/mo_krauti/wifi.py.git
cd wifi.py
chmod +x wifi.py
ln -s $(pwd)/wifi.py /some/dir/on/your/path
```

## Usage
Note: wifi.py must be run with root privileges.  
Examples:  
Start wpa_supplicant daemon for wlan0 interface
```
sudo wifi.py start wlan0
```
Check if wpa_supplicant daemon is listening on wlan0 and if wlan0 is connected to an AP
```
sudo wifi.py status wlan0
```
Stop wpa_supplicant daemon for wlan0 interface
```
sudo wifi.py stop wlan0
```
List nearby APs using wlan0 interface
```
sudo wifi.py list wlan0
```